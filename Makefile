#
# Makefile
#
# Copyright (C) 2018 subspaced (subspaced@gitlab)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# @brief Project GNU make file for SubSpace Market
#
#
all: subspaced

############ Dependencies, download and build static libraries ###############

### static OpenSSL ###
openssl:
	git clone https://github.com/openssl/openssl.git

openssl/Makefile: openssl
	cd openssl && ./Configure gcc -static no-shared no-threads no-zlib

lib/libcrypto.a: openssl/Makefile
	cd openssl && make
	@mkdir lib 2>/dev/null || true
	cp openssl/lib*a lib

### static curl ###
# requires autoconf and libtool
curl:
	git clone https://github.com/curl/curl.git

curl/Makefile: curl openssl
	cd curl && ./buildconf
	cd curl && LIBS="-ldl" CPPFLAGS="-I$(CURDIR)/openssl/include" LDFLAGS="-L$(CURDIR)/lib" ./configure --disable-shared --disable-thread --disable-pthreads --disable-threaded-resolver --disable-dict --disable-file --disable-ftp --disable-ftps --disable-gopher --disable-imap --disable-imaps --disable-ldap --disable-ldaps --disable-pop3 --disable-pop3s --disable-rtmp --without-librtmp --disable-rtsp --disable-smb --disable-smbs --disable-smtp --disable-smtps --disable-telnet --disable-tftp --disable-manual --disable-verbose --enable-proxy --without-ca-boundle --without-libidn2 --without-nghttp2 --without-zlib --without-libpsl

lib/libcurl.a: curl/Makefile
	cd curl && make
	@mkdir lib 2>/dev/null || true
	cp curl/lib/.libs/libcurl.a lib

### static libjpeg ###
libjpeg:
	curl http://www.ijg.org/files/jpegsrc.v9c.tar.gz -o libjpeg.tgz
	mkdir libjpeg && tar -C libjpeg --strip-components=1 -xzvf libjpeg.tgz
	rm libjpeg.tgz

libjpeg/Makefile: libjpeg
	cd libjpeg && ./configure --disable-shared --enable-static

lib/libjpeg.a: libjpeg/Makefile
	cd libjpeg && make
	@mkdir lib 2>/dev/null || true
	cp libjpeg/.libs/libjpeg.a lib

### static SQLite ###
sqlite3:
	mkdir sqlite3
	curl https://www.sqlite.org/2018/sqlite-amalgamation-3250000.zip -o sqlite3.zip
	unzip -d sqlite3 -j sqlite3.zip "*/sqlite3.c" "*/sqlite3.h"
	rm sqlite3.zip

### latest country list ###
data/countries.csv:
	curl https://datahub.io/core/country-codes/r/country-codes.csv -o data/countries.csv

libs: lib/libcrypto.a lib/libssl.a lib/libcurl.a lib/libjpeg.a src/sqlite3.c data/countries.csv

######################## subspace daemon compilation #########################
subspaced:
	@make -C src all

install: subspaced
	sudo cp -f subspaced /usr/bin

clean:
	@make -C src clean

cleanall: clean
	rm -rf lib bin subspaced || true

