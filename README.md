SubSpace Market
===============

```
Beam me down some goodies, Scotty!
```

SubSpace Market is a decentralized peer to peer network, a free anonym market which focuses on security and usability. Subspaced
is similar to OpenBazaar, but more secure and much simpler to use. It really brings the DNM for the masses. You run the subspaced
daemon on your local computer, and interface with the market in your browser just as you would with any other markets. All
communications within are automatically end-to-end encrypted with a military grade chipher, so you don't have to use PGP / GPG or
other third-party encryption tools with this market at all.

This is a TRULY FREE MARKET, there are no fees (except for the miner's fee). As being a decentralized system, we don't have to
keep track of balances, we have no servers to pay for, and the development is financed by donations only. So if you like it,
please donate!

  [BTC 2NCD9PWUzAnq4NJCNhgh43oDjvgvHT36QqN](bitcoin:2NCD9PWUzAnq4NJCNhgh43oDjvgvHT36QqN)

Simple Setup
------------

If you only plan to visit the market, look around, maybe occassionally buy something, then it's enough to have:

1. [Tor Browser Boundle](https://www.torproject.org/download/download-easy.html.en) (provides a web browser and an anonymizer as well. You just have to [enable bidirectional connections](https://gitlab.com/subspaced/subspaced/blob/master/docs/anonymizer.md).)
2. Any bitcoin wallet of your choosing, but the official [Bitcoin Core](https://gitlab.com/subspaced/subspaced/blob/master/docs/bitcoind.md) client wallet is recommended, as subspaced can connect to it.
3. [Download](https://gitlab.com/subspaced/subspaced/tree/master/bin), [configure](https://gitlab.com/subspaced/subspaced/blob/master/docs/configure.md) and [run](https://gitlab.com/subspaced/subspaced/blob/master/docs/running.md) the SubSpace Market daemon (easy to do, has a Configuration Assistant too).

Proper Setup
------------

As a seller (for your own protection) you'll want a full setup:

1. [Configure a VPN](https://gitlab.com/subspaced/subspaced/blob/master/docs/vpn.md) (optional, but strongly recommended)
2. [Configure torproxy or i2pd](https://gitlab.com/subspaced/subspaced/blob/master/docs/anonymizer.md) (the market's P2P network works on top of anonymizer networks)
3. [Configure bitcoind](https://gitlab.com/subspaced/subspaced/blob/master/docs/bitcoind.md) in daemon mode (optional, but recommended as it automates multisig wallet generation)
4. [Download subspaced](https://gitlab.com/subspaced/subspaced/tree/master/bin) (or [compile from source](https://gitlab.com/subspaced/subspaced/blob/master/docs/compile.md))
5. [Configure subspaced](https://gitlab.com/subspaced/subspaced/blob/master/docs/configure.md)
6. [Running subspaced](https://gitlab.com/subspaced/subspaced/blob/master/docs/running.md) and entering the SubSpace Market
7. Take OPSEC seriously

Manuals
-------

1. [Buyer's manual](https://gitlab.com/subspaced/subspaced/blob/master/docs/buyer.md)
2. [Seller's manual](https://gitlab.com/subspaced/subspaced/blob/master/docs/seller.md)
3. [Disputes and refund](https://gitlab.com/subspaced/subspaced/blob/master/docs/disputes.md)

Contributions
-------------

Your contributions are welcome.

1. Set up a [browse only](https://gitlab.com/subspaced/subspaced/blob/master/docs/browseonly.md) node to let others know about the network
2. Help by fixing [translations](https://gitlab.com/subspaced/subspaced/blob/master/docs/translate.md) or adding new languages
3. Provide your own Escrow Service
4. Donate if you can!

Legal statement
---------------

The authors of SubSpace Market do not encourage marketing any illegal goods. Our goal is to create a free marketplace safe from
greedy thiefy banks and corrupt governments who spend your taxes on oppression. We are not affiliated with the vendors on
the market in any way, therefore we take absolutely no responsibility for any merchandise on the market. We just provide the
marketplace software, and we do not make profit from the transactions.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Support
-------

We use the Issue Tracker to receive feedbacks from the users. As being a non-profit project, there's no guarantee that your problem
will be fixed in any time. But we're commited, and we are taking the tickets seriously. We prioritize them by the amount of
donation we've recevied along with the ticket, meaning if you make a generous donation, it's more likely your case will be
resolved sooner. Don't forget to put your donation's transaction id on the opening ticket. For your own safety, be sure that the
donation can't be connected to you or to your user account creating the ticket. We suggest to create a new wallet for every ticket,
and pay donations in Monero by using xmr.to.

