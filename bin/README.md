Download SubSpace Market daemon
===============================

SubSpace Market is a decentralized peer to peer network. It's daemon is distributed as a single executable binary,
which you can download from above. To figure out which version you need, in a terminal type `uname -mo`. Most desktop,
Tails OS and Whonix users will want *x86_64-GNU-Linux*.

We suggest to move the downloaded file to '/usr/local/bin/subspaced', but you can also run it from your home directory.
No installation required, but chances are good you have to grant execute permission on the file first:

```sh
$ chmod +x subspaced
```

!!! Check downloaded file's integrity !!!
-----------------------------------------

To ensure that no errors or malicious modification happened during the transfer, always check the file before you execute.
For that download the signature file next to it (ending in .sig), and copy'n'paste this into your terminal:

```sh
openssl dgst -sha512 -verify /dev/stdin -signature subspaced.sig subspaced << EOF
-----BEGIN PUBLIC KEY-----
MIIEIjANBgkqhkiG9w0BAQEFAAOCBA8AMIIECgKCBAEAvPtUvGod2bQFOjtxLaFJ
p1tplbBv/bZQ22uNxF1pSVaRlnDcAysVvVQbizp2uXoUoJYlbaJybvZ0HlrbkxcT
p93GY/HSPMZsK1zjGKhOdk7M6l1nswx1GHOjwePZm3uBCmIuRZzrvKGh+tWSTDmP
M+3rw1SFBgzecrs423VkiGQiOYuBm7/QhHW1UULcdLTN9hO+Upz86rZZvDRzxw0t
pq3hWgDWq2cOPg14pJYT0GliwTzFYx3dorEFq3CYrIEnb1JfJtlvxki1VIaWcBRE
/SQoiHg1IZj8sjE6O+D44Bdz9x+ROI8wz3WowGxTedyXRH9LHtzG+CzJWskLG/Wo
H1WZ7EqHD1JHwLNp46qdjfp9lhfnmCqlgbPE2WVExOczNzmh+7qh83LqW/+7u4Zx
g/o/N4rw3If1FKOQ4hVh9hTSkOjZoyq4Esxw+lzqcoF0LXoGjquLojKj3vnQMn1h
nvl14uRQ/rH1T3IW5Pd9TYlYYYnao07MmN/I47pOEZROuPQyefusJfkzW1tLCGB+
zY3TftgCf6ELnxmu6PCuViSHuewxZDyXUp67OPULoPdobfGNgdSEk8rdxN/BQujS
/O6tfzKzz1y6Ya/pNT9R+MxTkfrM+O3AR+onxgu5EoCdc8Wvmn/e9oVqoSOf792T
rq/GTr6tR2nga/9ArllLC7S8XbQTBYOHPhAHMO1iP6eDzrCpSlLlZ+kI52sfbQP9
/QUfiuNO0VgL0i15qWLI7WYUvh5zjudvFd80FLF9SAU6dkfoX007ZlkgYx3seG3M
Zd5fpFF1bXYvjpLS6v/hZRUAW13+EGnqfYkTmp5BTTWxj+BL90qwBgW0ewYw9owG
L+DheXoTXsDED/iSMNJR6UFUuRmbVzNalJ4CkjPqpbvvFLBy3acR8yyVkA/YxPij
qDOF0IQGPO+MBRcpytYs1HPaBtbQ2Mmv8Cl5ow/pcDomsjkJc4lGo/TVVNAL/TyV
vSWHX4PjWBXPx74bSvh/OnDL+v0/GiRar+Dk0UJrYasWin7MsFlQumAUn+B67CTL
g6UvOu8YLagCsxWxsGdLBOuye0uJuvOGNxMUW5oZVVhKMtGqGdFjVpSN3525Mur5
3NQMx2F+33J5U4LwhlHi+sTtbImSShTKIVtjSY7um8sguzKXHYEb3hdFYeGHsMqQ
ppKdpX9LulCN9J0kOko81OrRea/xmbjM31bKRxoi7EkbXJqn5QhLVxVWF6XDU0j4
1cqmaK1PqXJe7Ee9r8lCK9JItcsK1K0+2erqRWQv0s8NyunDuCmmjU7ngjH8KlJs
pDH7wXloNkNRK9CHRyQzMQtrwNLTLj4yor4/6cfH7LH8ZVhVGzoD9UsnY7EBEhNu
BwIDAQAB
-----END PUBLIC KEY-----
EOF

```

If nothing happens, make sure you've pressed an <kbd>Enter</kbd> at the end. If you see

```
Verified OK
```

then you will know that the executable wasn't tempered with, and it was compiled by the official SubSpace Market
Development Team indeed. And as such, you're safe to use it.

Other Required Software
-----------------------

The market's daemon is not all that you need! The daemon is using an anonymizer proxy to talk to other nodes, and a web
browser to interact with you. We recommended to use [TBB](https://www.torproject.org/download/download-easy.html.en).
For payments, you'll also need a [wallet application](https://gitlab.com/subspaced/subspaced/blob/master/docs/bitcoind.md).

If you encounter any problems, [read the documentation](https://gitlab.com/subspaced/subspaced/blob/master/README.md).

