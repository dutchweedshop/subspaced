SubSpace Market Beta Test
=========================

Dear Testers!

You have to set up a [tor](https://gitlab.com/subspaced/subspaced/blob/master/docs/anonymizer.md) Hidden Service first, then
please follow the guide here. A big WARNING, beta-test version only supports testnet wallets (starting with '2').

1. Blind Test
-------------

Without any further knowledge, [download](https://gitlab.com/subspaced/subspaced/tree/master/bin) and run subspaced. Let us know
if the Configuration Assistant was straightforward and has detected your configuration correctly.

Then try out the market, and let us know if the interface is intuitive enough. Designed mainly for TBB resolution, but the design
is responsive, should work with different window sizes. You can switch to dark theme by adding "darktheme=1" to the config file
'~/.subspaced/market.conf'. Search the products, try out different filters, and try to order something. Tell us if the multisig
guides for the buyer are understandable enough.

2. Seller Test
--------------

Second, set up a product to sell. Try to order again (you'll need another vm instance for that), and let us know if the
guides for the seller are good enough. Then configure JSON-RPC and run `bitcoind -regtest`, and repeat the procedure. This time
there should be no guides, and everything should be taken care for you. Let us know if everything worked well.

3. Through Test
---------------

Finally, [read all the documentation](https://gitlab.com/subspaced/subspaced/tree/master/docs), and try out all operating modes.
Use with TBB, with standalone tor, i2pd etc. Change languages, set up a browse only node, remore access, create an escrow product,
and try to use it (you'll need 3 vm instances: one for the buyer, one for the seller, and one for the escrow provider). Put as much
stress test as you want. If you can test subspaced under Tails OS or on a Raspberry, that's a very big plus.

4. Hack Test (for experts only)
-------------------------------

Try to hack into the site. This has two parts: one, from the web interface against a public browse only node (try sql injections,
malformed urls etc.). Do a DDoS against your node, and tell us how many connections it could take (being a decentralized network,
it's impossible to DDoS the entire market). Second, try to decipher the files under '~/.subspaced'. Little help: they have several
layers of encryption, the outtermost being AES. We did quite a good job at security, but this doesn't mean we've thought of
everything. Every sechole findings will be richly rewarded right on the spot provided you can present a working PoC for it.

Report Back Your Findings
-------------------------

If you find anything, open a ticket in the Issue Tracker on gitlab. We are also interested in positive feedbacks, eg. when
everything worked as it should, as well as in feature requests. Please don't open a ticket just to ask for another altcoin, that's
already on our roadmap. The most active testers with meaningful issues and valuable feedbacks will be rewarded once the market goes
on-line!

Thanks!

subspaced developers

