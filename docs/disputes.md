Disputes and Refund
===================

PREV: [Seller how-to](https://gitlab.com/subspaced/subspaced/blob/master/docs/seller.md)

Just like in a real life market, the one lenting the place for the seller takes no responsibility if the buyer and the seller
disagree.

We was thinking a lot how to implement the escrow. Finally we come up with a deposit system without any arbitrator, and letting
users to offer special Escrow Service products. Using only deposit will maximize security and enforce trust between the parties.
You see, if there's a third person, there's always a chance that the buyer or the seller works with them, or can mislead them,
therefore scamming became possible (not to mention famous market exit scams when that allegedly trusted arbitrator steals).
Besides SubSpace Market is a decentralized market, there's no central server to generate the escrow wallets and transactions
on, it has to be done on the buyer's and seller's side locally.

Delivery Failure
----------------

If the deal goes south and the goods are lost, then it is only fair if that's equally bad for everyone. The seller will loose
the product, and the buyer will loose the money. This guarantees the best intentions on both sides. Another advantage of
this system is, that there's NO MARKET FEE at all. The seller receives the whole amount that the buyer have paid (minus miner's
fee).

Buyer Must be Correct
---------------------

Because the money has to be pre-paid into a deposit box, the buyer can't steal the goods without paying. Also without a refund
option, there's no point in lying and denying a successful delivery and reclaiming the money. On the other hand if the buyer
receives something else than advertised, or the quality is nowhere near as promised, then the buyer has the right not to sign the
order, and the funds in the deposit box will be lost forever. If a certain buyer orders from several sellers, but never release
funds from the deposit, then the sellers can vote that buyer down to warn others.

Seller Must be Correct
----------------------

The only way to get the payment from the deposit box is with the buyer's permission. Therefore it is in the seller's best interest
not to scam the buyer, give the promised quality and do everything to make the package arrive at it's destination. If the buyer is
not satisfied, the seller won't receive any funds, and other future buyer's will be warned about the seller via the rating system.

Optional Refund
---------------

If the seller knows the buyer really well, and decides to give their money back, they can agree on it *OUTSIDE* of the market's
order process. The buyer has to generate a refund transaction, and the seller signs it with his BBBPRIV. But doing so makes it
possible for the buyer to scam the seller, therefore the SubSpace Market takes no responsibility, does not recommend nor encourage
this at all! That's why this has to be done outside of the normal order flow, discussed using only plain private messages.

Optional Escrow
---------------

As escrows make it possible for a seller to easily scam a buyer, they are not recommended nor encouraged by the market.
Nevertheless in some rare cases they could be useful, so SubSpace Market offers a way to sell special, Escrow Service products
by their users. Those products are not listed with the others, rather offered as an option during the order process. If one of
those is selected, then the full responsibility (including the creation of a 2-of-3 multisig wallet and generating the payout
transaction including the escrow's fee etc.) goes to the user offering the service, and the order afterwards is considered to be
handled *OUTSIDE* of the market. SubSpace Market only helps this by displaying a "Dispute" button on both the buyer and the seller
side, which will send a private message to the escrow vendor, but that's all.
*YOU HAVE BEEN WARNED NOT TO TRUST A THIRD PARTY WITH YOUR MONEY.*

