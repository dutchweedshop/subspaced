SubSpace Market Local Market Mode
=================================

The market software is designed to connect to one big global network by default. With this mode you can create many
independent local networks instead, which can be used by your neighbourhood exclusively. This mode is ideal to implement
small [local markets](https://www.thelawdictionary.org/local-market/), where the vendors are local manufacturers and
farmers, selling goods only to the locals without letting their oppressive governments and authorities know about it.

There's no way for us to monitor how many local networks are there, and we don't want to know. All we ask is if you
create such a network, then please [donate anonymously](https://gitlab.com/subspaced/subspaced/blob/master/README.md) to support
our developers and honour our cause.

Configuration
-------------

To enable this mode, and create a new market, set up a
[browse only](https://gitlab.com/subspaced/subspaced/blob/master/docs/browseonly.md) node but with `--configure -n` (note `-n`
and NOT `-b`). This will skip bootstrapping, and will add `localonly=1` to your '~/.subspaced/market.conf'. After that run the
subspaced daemon as usual.

The local vendors and customers should bootstrap their nodes from your new node, thus creating some sort of Local Area
Network if you like.

Interface
---------

In addition to the separated network, nodes that bootstrapped from a local market node will use a simplified web interface.

There will be no language translations, and "Shipping To" option will be omitted as well. Nodes will still be able to use
different languages on their web interface if they want, but products and their categories won't be translated.

Customers will also be able to select direct payment, without the need of creating a multisig wallet and deposit. This
is only allowed because the seller and the buyer supposed to know each other in person, and customers can break the legs
of scammer vendors.

Message of the Day
------------------

A "LOCAL MARKET" header will be shown above the motd. Therefore users will know if they bootstrap from that node, they
will NOT connect to the global market network.

NEXT: [Browse only mode](https://gitlab.com/subspaced/subspaced/blob/master/docs/browseonly.md)

