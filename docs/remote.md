Remote Access HOW-TO
====================

PREV: [Setting up a product profile](https://gitlab.com/subspaced/subspaced/blob/master/docs/product.md)

You can set up SubSpace Market so that you (and only you) can access your node remotely. For that,
you'll need a small computer (a virtual server or a Raspberry Pi hidden somewhere will do fine),
which is on the network all the time (henceforth called *node*), and a portable computer or tablet
(preferably a laptop with Tails OS and TBB) that you use to connect to it (called *client* hereafter).

Once you have configured an [anonymizer](https://gitlab.com/subspaced/subspaced/blob/master/docs/anonymizer.md)
and [subspaced](https://gitlab.com/subspaced/subspaced/blob/master/docs/configure.md) on the *node*,
add this line manually to your '~/.subspaced/market.conf':

```
token=SomethingOnlyYouRemeber
```

After that you will be able to connect to your *node* remotely using a tor Hidden Service or I2P
Server Tunnel. No further configuration needed, and you don't need to install subspaced on your
*client* either. Just point your TBB to:

```
https://(your node's onion or i2p address):8080/
```

And on the login screen, you'll be asked for the token too. Although it may seem like any other DNM,
note that only you, and no other users will be able to log in on your *node*'s website.

Security Considerations
-----------------------

Make sure you NEVER EVER run subspaced as root on your *node*. If you run it from '/etc/rc.local',
we strongly suggest to use `sudo -u subspaceduser subspaced`. Or in lack of sudo, you can drop root
privileges by specifying a 'user=' line in the
[configuration file](https://gitlab.com/subspaced/subspaced/blob/master/docs/configure.md).

We strongly recommend NOT to configure bitcoind on *node*, instead run a wallet application on your
*client* and copy'n'paste multisig wallet commands to the market. Yes, we know, copy'n'paste is less
comfortable than automation, but believe us, your security worth the price. Only install bitcoind if
you are 100% sure nobody except you can gain _physical_ access to your *node*, otherwise if somebody
steals your small computer, they will also steal your money. Using an encrypted volume to store bitcoind
files, setting up a very strict firewall and removing unnecessary system services are also a good idea.

It's important to run tor over a [VPN](https://gitlab.com/subspaced/subspaced/blob/master/docs/vpn.md)
on your *client*. This is crutial if you plan to use your *client* from public hotspots, like a pub or
coffee shop for example.

The connection between your *client* and *node* never leaves torland, and you have an extra encryption
layer with SSL, so you don't have to worry about someone stealing your credentials on an exit node. But
you will be vulnerable to phishing attacks, so always MAKE SURE OF IT, you're really connecting to YOUR
onion address, not some fake site. The market helps you with an url specific css scam warning (which only
works in certain browsers).

On authentication failure, there'll be a delay. On token failure, an additional extra long delay. This is
a protection against attackers trying to brute force your login credentials remotely.

SubSpace Market OS
------------------

If you find it troublesome to set up, configure and secure your *node*, you can always buy a pre-configured
image. Start up SubSpace Market daemon (locally on your machine for now), and look for "SubSpace Market OS".
Once purchased, you'll have a secured, firewalled, properly configured OS image with tor proxy and SubSpace
Market daemon pre-installed on it. Just write that image on an SD card or USB stick, and you are good to go!

NEXT: [Disputes and refund](https://gitlab.com/subspaced/subspaced/blob/master/docs/disputes.md)

