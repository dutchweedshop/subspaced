Translate HOW-TO
================

PREV: [Browse only mode](https://gitlab.com/subspaced/subspaced/blob/master/docs/browseonly.md)

Files
-----

In the source repository, you'll find a file named 'data/translate.csv'. This is a comma separated list encoded in UTF-8,
which you can open and save with OpenOffice, LibreOffice, Gnumeric, M$ Excel etc. As a last resort you can also edit it
with a plain text editor, just be careful not to mess up the syntax. When saving, always use comma (,) as separator, and
select enclose strings in quotes (") option. If you need quotes within strings, those must be encoded as double quotes ("")
and not escaped by backslash. For example: `L_PULPFICTIONGUY,"Samuel ""Motherfucker"" Jackson"`.

Each column is for one language. You can improve the translations, fix typos, or you can add an entirely new language.

The first six rows are special. The first row contains a two letter ISO-code for the language. The second is the name of
the language as known by it's users (so for example "русский" and NOT "Russian"). The third row can be either "ltr" or "rtl"
depending whether the writing system for that particular language is left-to-right or right-to-left. Finally, the last three
rows (lines 4-6) specify the date formats. Use libc's strftime formatting strings here. Type `man strftime` for help.
Please don't use special codes which may be not supported on some systems. Best to use %Y,%m,%d,%I,%p,%H,%M,%S,%z only.

Take extra care, not to change '%d' in the lines L_CHLDERR, L_PURGEMSG, L_SENDESNOT, L_DLEFT and L_CONFIRMOK. Some translator
likes to replace '%' with an identically looking, but different character. The correct one is ASCII 0x25, and 'd' (ASCII 0x64)
*follows* the percentage sign, not preceeds it (character order is specially important to right-to-left writing systems, which
probably display it as "d%"). Best practice is to replace '%d' with 'XX', translate, then replace back. If you keep this in mind,
and double check '%d' in a hexdump, there should be no problem.

Translation Help
----------------

It's quite often problematic for translators to figure out where a certain text will be displayed on the page. To help with
that, you can specify a special "??" language code.

```sh
$ subspaced -l ??
```

With this, you'll see the unique identifiers in the Configuration Assistant and on the webpage. Please note that this could
break some text formatting, but you'll be able to match a text in the application with the row in 'data/translate.csv' easily.

Translating Country Names
-------------------------

For that, you'll have to edit 'data/countries.csv'. For a new language, add a new column with the name 'official_name_(lang)',
where (lang) is the same two letter ISO-code as used in the first line of translate.csv. Watch out, this file is downloaded
when you refresh the country list, so your translations will be lost. We suggest to always keep a backup of this file, outside
of the 'data/' directory. Because they are not listed in this csv, the states of the USA cannot be translated, they are in English
only. Also you don't have to provide a translation if the name is the same as in English, like for example "Angola" or "Haiti".

User Provided Translation
-------------------------

You can add one language dictionary dynamically to subspaced by creating '~/.subspaced/translate.txt'. In that file, the first
line is a fallback language code, the other lines are identical to the rows of one column in 'data/translate.csv'. To create an
initial version of this file, use (assuming you want 'ru' as a fallback language):

```sh
$ subspaced -l ru --wrdict
```

Then you can edit '~/.subspaced/translate.txt' with a notepad. Note that this translation will only be seen by you, other nodes
won't be able to use your dictionary; also product names, descriptions and categories will be shown in the fallback language.

Applying the Changes
--------------------

First, create a user provided translation file. Translate, and check it in subspaced without the need of recompilation. When
that's okay, then replace the column or add a new one to 'data/translate.csv', without the fallback language code. For new
languages, also add the country names as a new column to 'data/counties.csv'.

Please note that you won't see the changes in the csv files instantly. You have to make a pull-request, we have to merge
your changes, recompile subspaced and users have to download the new version.

You should ALWAYS [compile](https://gitlab.com/subspaced/subspaced/blob/master/docs/compile.md) subspaced on your local computer
and check out the new translations a priori to any pull-request. Make extra checks on the translations that contain '%d'.
